An undirected graph of images on Flickr that share common metadata.
This dataset is built by forming links between images sharing common
metadata from Flickr.  Edges are formed between images from the same
location, submitted to the same gallery, group, or set, images sharing
common tags, images taken by friends, etc.  The original images are
collected from PASCAL, ImageCLEF, MIR, and NUS-wide.  Each line of the
file flickr.edge is an edge of the form "u v", which denotes that
images u and v share some common metadata.

The dataset was downloaded on 26th February 2013 from

http://snap.stanford.edu/data/web-flickr.html

If you use this dataset, please cite the following paper:

@inproceedings{DBLP:conf/eccv/McAuleyL12,
  author    = {Julian J. McAuley and Jure Leskovec},
  title     = {Image Labeling on a Network: Using Social-Network Metadata
               for Image Classification},
  booktitle = {ECCV (4)},
  year      = {2012},
  pages     = {828-841},
  ee        = {http://dx.doi.org/10.1007/978-3-642-33765-9_59},
  crossref  = {DBLP:conf/eccv/2012-4},
  bibsource = {DBLP, http://dblp.uni-trier.de}
}
@proceedings{DBLP:conf/eccv/2012-4,
  editor    = {Andrew W. Fitzgibbon and
               Svetlana Lazebnik and
               Pietro Perona and
               Yoichi Sato and
               Cordelia Schmid},
  title     = {Computer Vision - ECCV 2012 - 12th European Conference  on
               Computer Vision, Florence, Italy, October 7--13, 2012,
               Proceedings, Part IV},
  booktitle = {ECCV (4)},
  publisher = {Springer},
  series    = {Lecture Notes in Computer Science},
  volume    = {7575},
  year      = {2012},
  isbn      = {978-3-642-33764-2},
  ee        = {http://dx.doi.org/10.1007/978-3-642-33765-9},
  bibsource = {DBLP, http://dblp.uni-trier.de}
}

Nodes:   105,938
Edges: 2,316,948
