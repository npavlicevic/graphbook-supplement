Supplementary materials for the graph theory book at
https://code.google.com/p/graphbook/

Below is an explanation of the materials in this supplement.  Unless
otherwise stated, each graph should be considered as simple.  To
download a supplementary material, click on its name.

* `advogato <https://bitbucket.org/mvngu/graphbook-supplement/downloads/advogato.7z>`_
  -- a directed graph of the Advogato trust network.

* `amazon <https://bitbucket.org/mvngu/graphbook-supplement/downloads/amazon.7z>`_
  -- a directed graph of the Amazon product co-purchasing network.

* `celegans <https://bitbucket.org/mvngu/graphbook-supplement/downloads/celegans.7z>`_
  -- a directed graph of the neural network of a worm.

* `dblp <https://bitbucket.org/mvngu/graphbook-supplement/downloads/dblp.7z>`_
  -- an undirected graph of coauthorship in computer science.

* `dimes <https://bitbucket.org/mvngu/graphbook-supplement/downloads/dimes.7z>`_
  -- a directed graph of the Internet, derived from the DIMES project.

* `dolphin <https://bitbucket.org/mvngu/graphbook-supplement/downloads/dolphin.7z>`_
  -- an undirected graph of an interaction network of dolphins.

* `enron <https://bitbucket.org/mvngu/graphbook-supplement/downloads/enron.7z>`_
  -- an undirected graph of the Enron email network.

* `epinion <https://bitbucket.org/mvngu/graphbook-supplement/downloads/epinion.7z>`_
  -- a directed graph of the Epinion trust network.

* `flickr <https://bitbucket.org/mvngu/graphbook-supplement/downloads/flickr.7z>`_
  -- an undirected graph of a network of images on Flickr that share
  common metadata.

* `flixster <https://bitbucket.org/mvngu/graphbook-supplement/downloads/flixster.7z>`_
  -- an undirected graph of a friendship network on Flixster.

* `football <https://bitbucket.org/mvngu/graphbook-supplement/downloads/football.7z>`_
  -- an undirected graph of a network of team schedules in US college
  football.

* `friendster <https://bitbucket.org/mvngu/graphbook-supplement/downloads/friendster.7z>`_
  -- an undirected graph of a friendship network on Friendster.

* `gnutella <https://bitbucket.org/mvngu/graphbook-supplement/downloads/gnutella.7z>`_
  -- an undirected graph of the Gnutella peer-to-peer file sharing network.

* `google <https://bitbucket.org/mvngu/graphbook-supplement/downloads/google.7z>`_
  -- a directed graph of the WWW, data released by Google in 2002.

* `imdb <https://bitbucket.org/mvngu/graphbook-supplement/downloads/imdb.7z>`_
  -- an undirected graph of the network of film actors.

* `karate <https://bitbucket.org/mvngu/graphbook-supplement/downloads/karate.7z>`_
  -- an undirected graph of a friendship network in a karate club.

* `mathgene <https://bitbucket.org/mvngu/graphbook-supplement/downloads/mathgene.7z>`_
  -- a directed graph derived from the Mathematics Genealogy Project.

* `myspace <https://bitbucket.org/mvngu/graphbook-supplement/downloads/myspace.7z>`_
  -- an undirected graph of a friendship network on MySpace.

* `patent <https://bitbucket.org/mvngu/graphbook-supplement/downloads/patent.7z>`_
  -- a directed graph of a network of patent citation.

* `polblog <https://bitbucket.org/mvngu/graphbook-supplement/downloads/polblog.7z>`_
  -- a directed graph of a network of political blogs.

* `powergrid <https://bitbucket.org/mvngu/graphbook-supplement/downloads/powergrid.7z>`_
  -- an undirected graph of the network of the Western States Power Grid.

* `roadnet <https://bitbucket.org/mvngu/graphbook-supplement/downloads/roadnet.7z>`_
  -- an undirected graph of a road network of California.

* `stackexchange <https://bitbucket.org/mvngu/graphbook-supplement/downloads/stackexchange.7z>`_
  -- a directed graph of the network of question-and-response on Stack
  Exchange.

* `team <https://bitbucket.org/mvngu/graphbook-supplement/downloads/team.7z>`_
  -- a directed bipartite graph of an athlete-team network.

* `tencent <https://bitbucket.org/mvngu/graphbook-supplement/downloads/tencent.7z>`_
  -- a directed graph of a network of follower-followee on Tencent Weibo.

* `twitter <https://bitbucket.org/mvngu/graphbook-supplement/downloads/twitter.7z>`_
  -- a directed graph of a network of tweet-and-response on Twitter.

* `webeu <https://bitbucket.org/mvngu/graphbook-supplement/downloads/webeu.7z>`_
  -- a directed graph of the WWW under the top level domains ".hu" and ".eu".

* `wikipedia <https://bitbucket.org/mvngu/graphbook-supplement/downloads/wikipedia.7z>`_
  -- a directed graph of a network of page links on Wikipedia.

* `yeastprotein <https://bitbucket.org/mvngu/graphbook-supplement/downloads/yeastprotein.7z>`_
  -- an undirected graph of the protein interaction network of a yeast.

* `youtube <https://bitbucket.org/mvngu/graphbook-supplement/downloads/youtube.7z>`_
  -- an undirected graph of a friendship network on YouTube.
